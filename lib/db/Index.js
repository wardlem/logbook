const merge = require('merge-descriptors');
const _collect = require('stream-collector');
const {promisify} = require('util');
const pad = require('pad');
const moment = require('moment');

const collect = promisify(_collect);

const debug = require('debug')('peernet:Index');

const MAX_INT_DIGITS = String(Number.MAX_SAFE_INTEGER).length;


// much inspire: https://github.com/mafintosh/level-logs
function Index(options = {}) {
    const {
        db = null,
        keyEncoding = 'binary',
        valueEncoding = 'json',
        prefix = 'index:',
    } = options;

    return {
        db,
        prefix,
        keyEncoding,
        valueEncoding,
        __proto__: Index.prototype,
    };
}

merge(Index.prototype, {
    key(path) {
        let b = Buffer.alloc(this.prefix.length + path.length);
        b.write(this.prefix, 'utf8');
        b.write(path, this.prefix.length, 'utf8');

        return b;
    },
    unkey(key) {
        return key.toString('utf8', this.prefix.length);
    },
    entrykey(id) {
        return `entry:${pad(MAX_INT_DIGITS, String(id), '0')}`;
    },
    datekey(date, id) {
        return `date:${date.toISOString()}:${pad(MAX_INT_DIGITS, String(id), '0')}`;
    },
    range(start, end, limit = -1) {
        let startkey = this.key(start);
        let endkey = this.key(end);
        let reverse = startkey.compare(endkey) > 0;

        let values;
        if (reverse) {
            values = this.db.createValueStream({
                gt: this.key(end),
                lte: this.key(start),
                reverse,
                limit,
            });
        } else {
            values = this.db.createValueStream({
                gt: this.key(start),
                lte: this.key(end),
                reverse,
                limit,
            });
        }

        return collect(values);
    },
    keyrange(start, end, limit = -1) {
        let startkey = this.key(start);
        let endkey = this.key(end);
        let reverse = startkey.compare(endkey) > 0;

        let keys;
        if (reverse) {
            keys = this.db.createKeyStream({
                gt: this.key(end),
                lte: this.key(start),
                reverse,
                limit,
            });
        } else {
            keys = this.db.createKeyStream({
                gt: this.key(start),
                lte: this.key(end),
                reverse,
                limit,
            });
        }

        return collect(keys)
            .then((keys) => keys.map(this.unkey.bind(this)))
        ;
    },
    lastid() {
        return this.keyrange(this.entrykey(Number.MAX_SAFE_INTEGER), this.entrykey(0), 1)
            .then((lasts) => {
                if (lasts.length === 0) {
                    return 0;
                }

                return parseInt(this.unkey(lasts[0]).replace(/entry:0*/, ''));
            })
        ;
    },
    nextid() {
        return this.lastid().then((id) => id + 1);
    },
    put(entry) {
        return this.db.batch([
            {
                type: 'put',
                key: this.key(this.entrykey(entry.id)),
                value: entry,
            },
            {
                type: 'put',
                key: this.key(this.datekey(entry.date, entry.id)),
                value: entry.id,
            },
        ]);
    },
    listEntries(start = Number.MAX_SAFE_INTEGER, count = 10) {
        return this.range(this.entrykey(start), this.entrykey(0), count);
    },
});


module.exports = Index;
