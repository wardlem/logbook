const merge = require('merge-descriptors');
const {promisify} = require('util');

const _collect = require('stream-collector');
const collect = promisify(_collect);
const uint64be = require('uint64be');

const debug = require('debug')('logbook:Ledger');

// much inspire: https://github.com/mafintosh/level-logs
function Ledger(options = {}) {
    const {
        db = null,
        keyEncoding = 'binary',
        valueEncoding = 'json',
        prefix = 'ledger:',
    } = options;

    return {
        // path,
        db,
        prefix,
        keyEncoding,
        valueEncoding,
        __proto__: Ledger.prototype,
    };
}

merge(Ledger.prototype, {
    key(index) {
        let b = Buffer.alloc(this.prefix.length + 8);
        b.write(this.prefix, 'utf8');

        uint64be.encode(index, b, this.prefix.length);

        return b;
    },
    unkey(key) {
        return uint64be.decode(key, this.prefix.length);
    },
    range(start, end, limit = -1) {
        let keys;
        if (start <= end) {
            keys = this.db.createValueStream({
                gte: this.key(start),
                lt: this.key(end),
                reverse: false,
                limit,
            });
        } else {
            keys = this.db.createValueStream({
                gt: this.key(end),
                lte: this.key(start),
                reverse: true,
                limit,
            });
        }

        return collect(keys);
    },
    keyrange(start, end, limit = -1) {
        let keys;
        if (start <= end) {
            keys = this.db.createKeyStream({
                gte: this.key(start),
                lt: this.key(end),
                reverse: false,
                limit,
            });
        } else {
            keys = this.db.createKeyStream({
                gt: this.key(end),
                lte: this.key(start),
                reverse: true,
                limit,
            });
        }

        return collect(keys)
            .then((keys) => keys.map((this.unkey.bind(this))))
        ;
    },
    head() {
        return this.range(Number.MAX_SAFE_INTEGER, 0, 1)
            .then((lasts) => {
                return lasts.length ? lasts[0] : null;
            })
        ;
    },
    height() {
        return this.keyrange(Number.MAX_SAFE_INTEGER, 0, 1)
            .then((lasts) => {
                return lasts.length ? lasts[0] : 0;
            })
        ;
    },
    append(data) {
        return this.height()
            .then((height) => {
                return this.db.put(this.key(height + 1), data);
            })
        ;
    },
    get(index) {
        return this.db.get(this.key(index))
            .catch((e) => {
                if (e.notFound) {
                    return null;
                }

                return Promise.reject(e);
            })
        ;
    },
    resetTo(index) {
        return this.keyrange(Number.MAX_SAFE_INTEGER, index)
            .then((keys) => {
                const ops = keys.map((height) => {
                    return {type: 'del', key: this.key(height)};
                });

                return this.db.batch(ops);
            })
        ;
    },
    recent(n = 20) {
        return this.range(Number.MAX_SAFE_INTEGER, 0, n);
    },
});


module.exports = Ledger;
