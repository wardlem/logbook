const {EOL} = require('os');

const wordsToSentences = require('./wordsToSentences');
const date = require('date.js');

module.exports = dataFromText;
function dataFromText(text) {
    const data = {
        date: null,
        title: '',
        content: '',
    };

    let lines = splitLines(text);

    let firstLine;
    [firstLine, ...lines] = lines;

    let searchForDate = true;
    while (firstLine != null) {
        if (firstLine === '') {
            [firstLine, ...lines] = lines;
            continue;
        }

        const words = splitWords(firstLine);
        let sentences = wordsToSentences(words);

        let firstSentence = sentences[0];

        if (firstSentence == null) {
            continue;
        }

        if (searchForDate) {
            if (firstSentence[firstSentence.length - 1] === ':') {
                let dateString = firstSentence.substr(0, firstSentence.length - 1).replace(/( |^)at( )/, '$1@$2');
                data.date = date(dateString);
                sentences = sentences.slice(1);
                firstSentence = sentences[0];
            }

            searchForDate = false;
        }

        if (firstSentence != null) {
            data.title = firstSentence;
            lines = [sentences.join(' ')].concat(lines);
            break;
        }

        firstLine = lines.pop();
    }

    data.content = lines.join(EOL);

    return data;
}

function splitLines(text) {
    return text.split(/\r?\n/g);
}

function splitWords(text) {
    return text.split(/\s+/g);
}
