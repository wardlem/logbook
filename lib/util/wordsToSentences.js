const NORMAL_SENTENCE_TERMINATORS = new Set('.!?');
const SPECIAL_SENTENCE_TERMINATORS = new Set(':');

module.exports = wordsToSentences;

function wordsToSentences(args) {
    return args.reduce((res, arg) => {
        res[res.length - 1].push(arg);
        if (NORMAL_SENTENCE_TERMINATORS.has(arg[arg.length - 1])) {
            res.push([]);
        } else if (res.length === 1 && SPECIAL_SENTENCE_TERMINATORS.has(arg[arg.length - 1])) {
            res.push([]);
        }

        return res;
    }, [[]]).map((sentence) => sentence.join(' ')).filter(Boolean);
}
