const {homedir} = require('os');
const {promisify} = require('util');

const _mkdirp = require('mkdirp');
const merge = require('merge-descriptors');
const level = require('level');

const mkdirp = promisify(_mkdirp);

const Entry = require('./Entry');

const Ledger = require('./db/Ledger');
const Index = require('./db/Index');

module.exports = Journal;

function Journal(options) {
    const {
        path = `${homedir()}/.hippocamper`,
        name = 'journal',
    } = options;

    return {
        db: null,
        path,
        ledger: Ledger({
            path: `${path}/data`,
            prefix: `${name}:ledger:`,
        }),
        index: Index({
            path: `${path}/data`,
            prefix: `${name}:index:`,
        }),
        __proto__: Journal.prototype,
    };
}

merge(Journal.prototype, {
    init() {
        return this.open()
            .then(() => {
                this.ledger.db = this.db;
                this.index.db = this.db;

                return this;
            })
        ;
    },
    open() {
        if (this.db == null) {
            return mkdirp(`${this.path}/data`)
                .then(() => {
                    this.db = level(`${this.path}/data`, {
                        keyEncoding: 'binary',
                        valueEncoding: 'json',
                    });
                })
                .then(() => this)
            ;
        } else if (this.db.isClosed()) {
            return this.db.open().then(() => this);
        }

        return Promise.resolve(null);
    },
    close() {
        if (this.db == null || this.db.isClosed()) {
            return Promise.resolve(null);
        }

        return this.db.close();
    },
    createEntry(data) {
        return this.index.nextid()
            .then((id) => {
                const entry = Entry(Object.assign({}, data, {id}));
                console.log(entry);
                return this.index.put(entry);
            })
        ;
    },
    listEntries() {
        return this.index.listEntries()
            .then((entries) => entries.map(Entry))
        ;
    },
});
