const merge = require('merge-descriptors');

module.exports = Entry;

function Entry(data) {
    let {
        id = null,
        date,
        title = '',
        content = '',
        created = new Date(),
    } = data;

    if (typeof date === 'string') {
        date = new Date(created);
    }

    if (typeof created === 'string') {
        created = new Date(created);
    }

    return {
        id,
        date,
        title,
        content,
        created,
        __proto__: Entry.prototype,
    };
}

merge(Entry.prototype, {
    toJSON() {
        return {
            id: this.id,
            date: this.date.toISOString(),
            title: this.title,
            content: this.content,
            created: this.created.toISOString(),
        };
    },
});
